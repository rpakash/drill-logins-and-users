const fs = require("fs/promises");
const path = require("path");
const fsCallback = require("fs");
const { resolve } = require("path");

function writeFile(path, data) {
  return fs.writeFile(path, data);
}

function deleteFile(path) {
  return fs.unlink(path);
}

function readFile(path) {
  return fs
    .readFile(path)
    .then((data) => {
      return data;
    })
    .catch((err) => {
      throw new Error(err);
    });
}

function fetchData(url) {
  return fetch(url)
    .then((data) => {
      if (data.ok) {
        return data.text();
      } else {
        throw new Error(err);
      }
    })
    .then((data) => {
      return data;
    })
    .catch((err) => {
      throw new Error(err);
    });
}

function problem1() {
  let fileNames = Array(2)
    .fill(0)
    .map(() => {
      return `${Math.round(Math.random() * 10)}.txt`;
    });

  let writeFilePromise = fileNames.map((file) => {
    return writeFile(path.join(__dirname, file), file);
  });

  Promise.allSettled(writeFilePromise)
    .then((data) => {
      console.log(data);
      console.log("File write operation completed");

      return new Promise((resolve, reject) => {
        setTimeout(() => {
          console.log("2 sec completed");

          resolve();
        }, 2000);
      });
    })
    .then(() => {
      let deleteFilePromise = fileNames.map((file) => {
        return deleteFile(path.join(__dirname, file), file);
      });

      return Promise.allSettled(deleteFilePromise);
    })
    .then((data) => {
      console.log(data);
      console.log("Delete operation completed");
    })
    .catch((err) => {
      console.log("An error occured");
      console.log(err);
    })
    .finally(() => {
      console.log("Process completed");
    });
}

function problem2WithPromise() {
  fetchData("https://loripsum.net/api/verylong/plaintext")
    .then((data) => {
      // console.log(data);
      return writeFile(path.join(__dirname, "lipsum.txt"), data);
    })
    .then(() => {
      return readFile(path.join(__dirname, "lipsum.txt"));
    })
    .then((data) => {
      return writeFile(path.join(__dirname, "anotherLipsum.txt"), data);
    })
    .then(() => {
      return deleteFile(path.join(__dirname, "lipsum.txt"));
    });
}

function writeFileCallback(path, writeData, callback) {
  fsCallback.writeFile(path, writeData, (err) => {
    if (err) {
      callback(err);
    } else {
      callback(null);
    }
  });
}

function readFileCallback(path, callback) {
  fsCallback.readFile(path, (err, data) => {
    if (err) {
      callback(err, null);
    } else {
      callback(null, data);
    }
  });
}

function deleteFileCallback(path, callback) {
  fsCallback.unlink(path, (err) => {
    if (err) {
      callback(err);
    } else {
      callback(null);
    }
  });
}

function problem2WithCallback() {
  fetchData("https://loripsum.net/api/verylong/plaintext").then((fetchData) => {
    writeFileCallback(path.join(__dirname, "lipsum.txt"), fetchData, (err) => {
      if (err) {
        throw new Error(err);
      } else {
        readFileCallback(path.join(__dirname, "lipsum.txt"), (err, lipsumData) => {
          if (err) {
            throw new Error(err);
          } else {
            writeFileCallback(path.join(__dirname, "anotherLipsum.txt"), lipsumData, (err) => {
              if (err) {
                throw new Error(err);
              } else {
                deleteFileCallback(path.join(__dirname, "lipsum.txt"), (err) => {
                  if (err) {
                    throw new Error(err);
                  }
                });
              }
            });
          }
        });
      }
    });
  });
}

function appendToFile(path, data) {
  return fs.appendFile(path, data);
}

function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}

function logData(user, id, activity) {
  let status = {
    loginSuccess: "Login Success",
    loginFailed: "Login Failure",
    dataSuccess: "GetData Success",
    dataFailed: "GetData Failure",
  };

  return appendToFile(path.join(__dirname, "log.txt"), `\n ${user} - ${id} - ${status[activity]} - ${new Date()}`);

  // use promises and fs to save activity in some file
}

function loginUser(user, id) {
  login(user, id)
    .then(() => {
      return getData();
    })
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

function getUserWithId(usersData, user, id) {
  return new Promise((resolve, reject) => {
    let userData = usersData.filter((user) => {
      return user.id === id;
    });
    if (userData.length !== 0) {
      resolve(userData[0]);
    } else {
      return logData(user, id, "dataFailed").finally(() => {
        reject("User data not found");
      });
    }
  });
}

function loginAndGetdata(user, id) {
  login(user, id)
    .then((userName) => {
      return logData(userName, id, "loginSuccess");
    })
    .catch(() => {
      return logData(user, id, "loginFailed").finally(() => {
        throw new Error("Login failed");
      });
    })
    .then(() => {
      return getData();
    })
    .then((usersData) => {
      return getUserWithId(usersData, user, id);
    })
    .then(() => {
      return logData(user, id, "dataSuccess");
    })
    .catch((err) => {
      console.log(err);
    });
}
